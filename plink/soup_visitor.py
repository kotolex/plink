from bs4 import BeautifulSoup
import requests
from plink.visitor import Visitor


class SoupVisitor(Visitor):
    """
    Concrete visitor class, which uses Beautiful Soup to parse page content
    """

    def __init__(self, url: str):
        super().__init__(url)
        self.soup = BeautifulSoup

    def visit(self) -> set:
        page = requests.get(self._url)
        self.soup = BeautifulSoup(page.content, 'html.parser')
        set_img = self._get_links('img', 'src')
        set_a = self._get_links('a', 'href')
        return {line for line in (set_img | set_a) if line.startswith('http')}

    def _get_links(self, tag_name: str, attr_name: str) -> set:
        return {self._format_link(link.get(attr_name)) for link in self.soup.find_all(tag_name)
                if link.get(attr_name) and not link.get(attr_name).startswith('mailto')}
