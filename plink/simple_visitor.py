import re
from plink.visitor import Visitor
import urllib.request


class SimpleVisitor(Visitor):
    """
    Concrete visitor class, which uses standart urllib library to parse page content
    """

    def __init__(self, url):
        super().__init__(url)
        self._url = url

    def visit(self) -> set:
        with urllib.request.urlopen(self._url) as response:
            try:
                the_page = response.read().decode('UTF-8')
            except UnicodeDecodeError:
                return set()
            return self._get_links(the_page)

    def _get_links(self, text) -> set:
        href_r = re.compile(' href=\"(.*?)\"')
        src_r = re.compile(' src=\"(.*?)\"')
        return self._make_set(href_r.findall(text)) | self._make_set(src_r.findall(text))

    def _make_set(self, iter: list) -> set:
        """
        Creates a set of formatted links, except mailto ones
        :param iter: list of the links (urls)
        :return: set
        """
        return {self._format_link(link) for link in iter if not link.startswith('mailto')}
