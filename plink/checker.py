import time
import requests
import threading
from plink.soup_visitor import SoupVisitor
from plink.simple_visitor import SimpleVisitor


class Spider:
    """
    Represents class for checking all links (images, sources, etc.) on a web-site, including all inner pages,
    which starts with the same domain. Checking links means it availability, responses HTTP codes from 200 to
    307. Third-party links will be checked, but not visited.
    Can use two different Visitors to parse page content: Simple, which uses standart library, and Soup, which
    uses BeautifulSoup library
    """

    def __init__(self, url, need_logging=True, using='simple'):
        """
        Main constructor, which gets page url of site (it must be main site page) to check all links
        :param url: page url, starting with http(s), main site page
        :param need_logging: need or not to print results to console, default is True
        :param using: which visitor to use (simple or soup)
        """
        self._url = url
        # collects all broken urls
        self.broken = dict()
        # collects all visited urls
        self.visited = list()
        # collects all checked urls
        self.checked = list()
        # collects all urls we need to check
        self._to_check = list()
        self._main_domain = self._url
        self.need_logging = need_logging
        self.total_time = 0
        # get visitor, default id Simple
        self.__visitor = {'simple': SimpleVisitor, 'soup': SoupVisitor}.get(using, SimpleVisitor)
        self.__threads = []
        # excluding list, mean not to visit pages, which ends with
        self._ext_list = ['gif', 'jpg', 'png', 'js', 'css', 'xml', 'zip', 'txt', 'java', 'avi', 'jar', 'ico', 'rar']

    def start(self) -> dict:
        """
        Starts checking the main page and all other site pages. Although it uses multithreading, it take some
        time, depending on site pages number, and number of links on that pages. So, be patient.
        :return: dictionary of results, including broken links and time elapsed
        """
        self._to_check.append(self._url)
        start = time.time()
        while self._to_check or self.__is_any_tread_alive():
            if self._to_check:
                new_page = self._to_check.pop()
                if self.need_logging:
                    print(f"Visit {new_page}")
                self.visited.append(new_page)
                links = self._get_all_links(new_page)
                thread = threading.Thread(target=self._check_all_links, args=[links, new_page])
                self.__threads.append(thread)
                thread.start()
        self.total_time = round((time.time() - start), 2)
        if self.need_logging:
            self.print_result()
        return {"checked": self.checked, "visited": self.visited, "broken": self.broken, "time": self.total_time}

    def print_result(self) -> None:
        """
        Print results of the checking
        """
        print(f'Total time elapsed: {self.total_time} seconds')
        print(f'Visited: {len(self.visited)}')
        print(f'Checked: {len(self.checked)}')
        print(f'Broken: {len(self.broken)}')
        if self.broken:
            print('Broken links are (<link> - <page>:')
            for k, v in self.broken.items():
                print(k, ' - ', v)

    def _check_all_links(self, links: set, new_page: str) -> None:
        """
        Checks all links on page, removes anchors from it, and sort them
        Do not use this method directly!
        :param links: set of links to check
        :param new_page: source web page
        """
        for link in links:
            # remove last slash
            link = link[:-1] if link.endswith("/") else link
            # remove anchors
            link = link.split('#')[0] if '#' in link else link
            if link not in self.checked:
                self.checked.append(link)
                self._sort_link(link, new_page)

    def _sort_link(self, link: str, new_page: str) -> None:
        """
        If link is broken, puts it to broken collection. Otherwise, checks if it in same domain (on site),
        and if it so puts it to to_check collection
        Do not use this method directly!
        :param link: link
        :param new_page: domain
        """
        if self._is_alive(link):
            if link.startswith(self._main_domain) and link not in self.visited and not link.split(
                    '.').pop() in self._ext_list:
                self._to_check.append(link)
        else:
            self.broken[link] = new_page

    def _get_all_links(self, url: str) -> set:
        """
        Get links collection(set) using current visitor.
        Do not use this method directly
        :param url: str representation of page
        :return: set of links
        """
        return self.__visitor(url).visit()

    def _is_alive(self, url: str) -> bool:
        """
        Checks if link(url) is available - return HTTP code from 200 to 307
        No reasons to use it directly
        :param url: str representation of page url
        :return: True if link is available, False otherwise
        """
        try:
            return 200 <= requests.head(url).status_code <= 307
        except Exception:
            return False

    def __is_any_tread_alive(self) -> bool:
        """
        Checks for all threads are done their work
        :return: True if some thread is still working, False otherwise
        """
        for thread in self.__threads:
            if thread.is_alive():
                return True
        return False


if __name__ == '__main__':
    s = Spider("http://skipy.ru", using="soup")
    s.start()
