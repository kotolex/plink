from abc import ABCMeta, abstractmethod
from urllib.request import urljoin


class Visitor(metaclass=ABCMeta):
    """
    Represent visitor class, who must get url content and extract all available links (urls) from it
    """

    def __init__(self, url: str):
        """
        Init object with target page url
        :param url: str representation of url, starting with http(s)
        """
        self._url = url

    def _format_link(self, link: str) -> str:
        """
        'Private' method to check right format of the url. If it not starts with http(s), it will
        form it and return
        :param link: url link to check
        :return: url link which starts with http(s)
        """
        return link if link.startswith("http") else urljoin(self._url, link)

    @abstractmethod
    def visit(self) -> set:
        """
        Main interface method to get all links from target page
        :return: set of links from target url
        """
        pass

    @abstractmethod
    def _get_links(self, *args) -> set:
        """
        'Private' method for getting links with some parameters. No reasons to access it directly!
        :param args: some parameters to parse links from page content
        :return: set of links
        """
        pass
